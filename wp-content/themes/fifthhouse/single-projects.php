<?php
get_header('dark');

while(have_posts()):
	the_post();
	
	$gallery = get_field('project_gallery');
?>
<div class="services-content">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-left project-info">
					<h1><?php the_title(); ?></h1>
					<div class="row">
						<div class="col-sm-6">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row gallery">
				<?php foreach($gallery as $img): ?>
				<div class="col-sm-4">
					<img src="<?php echo $img['url']; ?>">
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
<?php
endwhile;

get_footer();
?>