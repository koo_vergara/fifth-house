<?php
/* Template Name: Works */
get_header('dark');
?>
<div class="services-content">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1><?php echo get_the_title(); ?></h1>
				</div>
			</div>
			<?php get_template_part('content', 'projects'); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>