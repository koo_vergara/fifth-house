<?php
/* Template Name: About */
get_header('dark');
?>
<div class="about-content">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1><?php echo get_the_title(); ?></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-8"><?php echo get_field('upper_section'); ?></div>
			</div>
		</div>
		<img src="<?php echo get_field('image_section')['url']; ?>">
		<div class="container">
			<div class="row">
				<div class="col-sm-8"><?php echo get_field('lower_section'); ?></div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>