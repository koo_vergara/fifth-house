<?php
/* Template Name: Contact Us */
get_header('dark');
?>
<div class="contact">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h1><?php echo get_the_title(); ?></h1>
				</div>
			</div>
			<?php get_template_part('content', 'contact'); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>