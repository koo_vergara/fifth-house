<?php
$args = array(
	'post_type'			=> 'projects',
	'posts_per_page'	=> 9,
	'meta_query'		=> array(
		array(
			'key'	=> '_is_ns_featured_post',
			'value' => 'yes'
		)
	)
);
$projects = new WP_Query($args);
$newList = [];

foreach($projects->posts as $project){
	$ord = get_field('project_masonry_order', $project->ID);
	$newList[$ord-1] = ['order' => $ord, 'data' => $project];
}

ksort($newList);
?>
<div class="row project">
<?php
for($c = 0; $c < 3; $c++):
	$del = 0;
?>
	<div class="col-sm-4">
		<?php
		foreach($newList as $k => &$item):
			if($k < 3):
				$img = wp_get_attachment_image_src(get_post_thumbnail_id($item['data']->ID), 'large');
				$category = get_the_category($item['data']->ID)[0]->name;
		?>
		<div class="row">
			<div class="col-sm-12">
				<div class="masonry-grid masonry-grid-<?php echo $item['order']; ?>">
					<div class="project-overlay">
						<div class="project-overlay-text text-center">
							<p class="project-name"><a href="<?php echo get_the_permalink($item['data']->ID); ?>"><?php echo $item['data']->post_title; ?></a></p>
							<p class="project-category"><?php echo $category; ?></p>
						</div>
					</div>
					<img src="<?php echo $img[0]; ?>">
				</div>
			</div>
		</div>
		<?php 
				$del = $k;
			endif;
		endforeach;
		
		array_splice($newList, 0, 3);
		?>
	</div>
<?php endfor; ?>
</div>