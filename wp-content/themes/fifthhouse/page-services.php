<?php
/* Template Name: Services */
get_header('dark');

$services = array_slice(array_reverse(get_terms('category', array('parent' => 0))), 1);
?>
<div class="services-content">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1><?php echo get_the_title(); ?></h1>
				</div>
			</div>
			<?php
			foreach($services as $key => $service):
				if($key == 0 || $key == 2):
			?>
			<div class="row service-img-wrap">
			<?php endif; ?>
			<?php if($key == 0 || $key == 3): ?>
				<div class="col-sm-4">
			<?php else: ?>
				<div class="col-sm-8">
			<?php endif; ?>
					<div class="service-img">
						<div class="project-overlay">
							<div class="project-overlay-text text-center">
								<p class="project-name"><a href="<?php echo esc_url(get_term_link($service)); ?>"><?php echo $service->name; ?></a></p>
							</div>
						</div>
						<img src="<?php echo z_taxonomy_image_url($service->term_id); ?>">
					</div>
				</div>
			<?php if($key == 1 || $key == 3): ?>
			</div>
			<?php endif; ?>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>