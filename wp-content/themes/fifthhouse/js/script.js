$(function(){
	$('.show-navi').on('click', function(){
		$('.navi').css('right', '0');
	});

	$('.cancel-navi').on('click', function(){
		$('.navi').css('right', '-100%');
	});

	$(document).on('click', function(e){
		var navi = $('.navi');

		if(navi.css('right') == '0px'){
			if(!navi.is(e.target) && navi.has(e.target).length === 0){
	    		navi.css('right', '-100%');
	    	}
		}
	});

	$('.down a').on('click', function(){
		if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname){
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			
			if(target.length){
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});
});