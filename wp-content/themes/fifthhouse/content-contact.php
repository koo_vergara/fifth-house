<?php $options = get_option('fh_settings'); ?>
<div class="row">
	<div class="col-sm-5 text-left">
		<h2 <?php echo is_front_page() ? '' : 'class="contact-info-title"'; ?>>Contact Info:</h2>
		<p class="contact-desc"><?php echo $options['fh_contact_description']; ?></p>
		<p><strong>Address:</strong> <?php echo $options['fh_contact_address']; ?></p>
		<p><strong>Phone:</strong> <?php echo $options['fh_contact_phone']; ?></p>
		<p><strong>Email:</strong> <?php echo $options['fh_contact_email']; ?></p>
		<p><strong>Fax:</strong> <?php echo $options['fh_contact_fax']; ?></p>
	</div>
	<div class="col-sm-7">
		<?php echo do_shortcode('[contact-form-7 id="40" title="Contact Form"]'); ?>
	</div>
</div>