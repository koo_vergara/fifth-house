<?php get_header('dark'); ?>
<?php
$category = get_queried_object();
$subcat = get_term_children($category->cat_ID, 'category');
$args = array(
	'post_type'			=> 'projects',
	'category_name'		=> $category->slug,
	'posts_per_page' 	=> -1,
	'orderby'			=> 'date',
	'order'				=> 'DESC'
);
?>
<div class="services-content">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 <?php echo ($category->category_parent > 0) ? 'service-gallery' : ''; ?>">
					<h1><?php echo $category->name; ?></h1>
					<?php if($category->category_parent > 0): ?>
					<div class="row">
						<div class="col-sm-6">
							<p><?php echo $category->description; ?></p>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="row">
				<?php
				if(empty($subcat)):
					$posts = new WP_Query($args);

					get_template_part('content', 'works');
				else:
					foreach($subcat as $cat):
						$child = get_term_by('id', $cat, 'category');
				?>
				<div class="col-sm-4 subcat">
					<section class="subcat-info">
						<div class="row">
							<div class="col-sm-12">
								<a href="<?php echo esc_url(get_term_link($child)); ?>"><img src="<?php echo z_taxonomy_image_url($child->term_id); ?>"></a>
							</div>
						</div>
						<div class="row cat-desc">
							<div class="col-sm-12">
								<a href="<?php echo esc_url(get_term_link($child)); ?>"><h4><?php echo $child->name; ?></h4></a>
								<p><?php echo wp_trim_words($child->description, 20, '...'); ?></p>
							</div>
						</div>
					</section>
				</div>
				<?php
					endforeach;
				endif;
				?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>