<?php

add_theme_support('post-thumbnails');

function register_my_menu(){
	register_nav_menu('header-menu', __('Header Menu'));
}
add_action('init', 'register_my_menu');

function fifthhouse_scripts(){
	wp_enqueue_style('bootstrap-style', get_template_directory_uri().'/css/bootstrap.min.css', array(), '3.3.7');
	wp_enqueue_style('fifthhouse-fonts', get_template_directory_uri().'/css/fonts.css', array(), '');
	wp_enqueue_style('fifthhouse-style', get_template_directory_uri().'/css/style.css', array('bootstrap-style'), '');
	wp_enqueue_script('jquery-script', get_template_directory_uri().'/js/jquery-3.2.0.min.js', array(), '3.2.0', true);
	// wp_enqueue_script('parallax-script', get_template_directory_uri().'/js/parallax.min.js', array(), '', true);
	wp_enqueue_script('bootstrap-script', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery-script'), '3.3.7', true);
	wp_enqueue_script('fifthhouse-script', get_template_directory_uri().'/js/script.js', array(), '', true);
}
add_action('wp_enqueue_scripts', 'fifthhouse_scripts');

function set_custom_edit_projects_columns($columns){
    $columns['project_masonry_order'] = __('Masonry Order');

    return $columns;
}
add_filter('manage_projects_posts_columns', 'set_custom_edit_projects_columns');

function custom_projects_column($column, $post_id){
	// var_dump(get_field('project_masonry_order'));
	if($column == 'project_masonry_order'){
		echo get_field('project_masonry_order');
	}
}
add_action('manage_projects_posts_custom_column', 'custom_projects_column', 10, 2);

function add_masonry_guide($post_type){
	$screen = get_current_screen();

	if('edit-projects' === $screen->id){
		add_action('all_admin_notices', function(){
?>
<style>
.mason-grid { width:40px; height:30px; margin-bottom:10px; background-color:#ccc; display:table; text-align:center; }
.mason-grid p { margin:0; display:table-cell; vertical-align:middle; }
.double-grid { height:70px; }
</style>
<div style="max-width:150px;">
	<p style="font-size:16px;"><strong>Masonry Grid Guide</strong></p>
	<div style="float:left; width:40px; margin-right:10px;">
		<div class="mason-grid"><p>1</p></div>
		<div class="mason-grid"><p>2</p></div>
		<div class="mason-grid double-grid"><p>3</p></div>
	</div>
	<div style="float:left; width:40px; margin-right:10px;">
		<div class="mason-grid double-grid"><p>4</p></div>
		<div class="mason-grid"><p>5</p></div>
		<div class="mason-grid"><p>6</p></div>
	</div>
	<div style="float:left; width:40px;">
		<div class="mason-grid"><p>7</p></div>
		<div class="mason-grid double-grid"><p>8</p></div>
		<div class="mason-grid"><p>9</p></div>
	</div>
	<div style="clear:both;"></div>
</div>
<?php
        });
    }
}
add_action('load-edit.php', 'add_masonry_guide');

function fh_add_admin_menu(){
	add_options_page('Contact Info', 'Contact Info', 'manage_options', 'contact_info', 'fh_options_page');
}
add_action('admin_menu', 'fh_add_admin_menu');

function fh_settings_init(){
	register_setting('pluginPage', 'fh_settings');

	add_settings_section(
		'fh_pluginPage_section',
		__('Test', 'wordpress'),
		'fh_settings_section_callback',
		'pluginPage'
	);

	add_settings_field(
		'fh_contact_description',
		__('Description', 'wordpress'),
		'fh_contact_description_render',
		'pluginPage',
		'fh_pluginPage_section'
	);

	add_settings_field(
		'fh_contact_address',
		__('Address', 'wordpress'),
		'fh_contact_address_render',
		'pluginPage',
		'fh_pluginPage_section'
	);

	add_settings_field(
		'fh_contact_phone',
		__('Phone', 'wordpress'),
		'fh_contact_phone_render',
		'pluginPage',
		'fh_pluginPage_section'
	);

	add_settings_field(
		'fh_contact_email',
		__('Email', 'wordpress'),
		'fh_contact_email_render',
		'pluginPage',
		'fh_pluginPage_section'
	);

	add_settings_field(
		'fh_contact_fax',
		__('Fax', 'wordpress'),
		'fh_contact_fax_render',
		'pluginPage',
		'fh_pluginPage_section'
	);
}
add_action('admin_init', 'fh_settings_init');


function fh_contact_description_render(){
	$options = get_option('fh_settings');
	?>
	<textarea cols="100" rows="5" name="fh_settings[fh_contact_description]"><?php echo $options['fh_contact_description']; ?></textarea>
	<?php
}

function fh_contact_address_render(){
	$options = get_option('fh_settings');
	?>
	<input type="text" name="fh_settings[fh_contact_address]" value="<?php echo $options['fh_contact_address']; ?>">
	<?php
}

function fh_contact_phone_render(){
	$options = get_option('fh_settings');
	?>
	<input type="text" name="fh_settings[fh_contact_phone]" value="<?php echo $options['fh_contact_phone']; ?>">
	<?php
}

function fh_contact_email_render(){
	$options = get_option('fh_settings');
	?>
	<input type="text" name="fh_settings[fh_contact_email]" value="<?php echo $options['fh_contact_email']; ?>">
	<?php
}

function fh_contact_fax_render(){
	$options = get_option('fh_settings');
	?>
	<input type="text" name="fh_settings[fh_contact_fax]" value="<?php echo $options['fh_contact_fax']; ?>">
	<?php
}

function fh_settings_section_callback(){ 
	echo __('This section description', 'wordpress');
}

function fh_options_page(){
?>
	<form action='options.php' method='post'>
		<h1>Contact Info</h1>
		<?php
		settings_fields('pluginPage');
		do_settings_sections('pluginPage');
		submit_button();
		?>
	</form>
	<?php
}