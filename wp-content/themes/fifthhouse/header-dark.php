<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width">
		<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<!--[if lt IE 9]>
		<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
		<![endif]-->
		<?php wp_head(); ?>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</head>
	<body <?php body_class(); ?>>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-xs-8 logo">
						<a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri().'/images/fifthhouse_logo_dark.png'; ?>"></a>
					</div>
					<div class="col-sm-6 col-xs-4 menu">
						<div class="burger">
							<p class="text-right"><a href="javascript:void(0)" class="show-navi"><img src="<?php echo get_template_directory_uri().'/images/burger_dark.png'; ?>"></a></p>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="navi">
			<p class="text-right"><a href="javascript:void(0)" class="cancel-navi"><img src="<?php echo get_template_directory_uri().'/images/cancel.png'; ?>"></a></p>
			<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
		</div>