<?php
/* Template Name: Home */
get_header();
?>
<div class="banner">
	<div class="container">
		<div class="title">
			<p class="text-center">Neque porro quisquam est qui</p>
			<h1 class="text-center">Lorem ipsum dolor sit amet.</h1>
		</div>
		<div class="down">
			<a href="#scroll-anchor"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a>
		</div>
	</div>
</div>
<div id="scroll-anchor" class="container">
	<div class="row">
		<div class="col-sm-4 col-sm-offset-4 text-center">
			<div class="fifthhouse-icon"></div>
		</div>
	</div>
</div>
<?php foreach(get_field('home_content') as $home_content): ?>
<div class="home-content container">
	<div class="row">
		<div class="col-sm-12">
			<h2 class="text-center"><?php echo $home_content['home_content_section_title']; ?></h2>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2 text-center"><?php echo $home_content['home_content_section_content']; ?></div>
	</div>
</div>
<?php endforeach; ?>
<div class="home-content container"><?php get_template_part('content', 'projects'); ?></div>
<div class="testimonials">
	<div class="container"><?php get_template_part('content', 'testimonials'); ?></div>
</div>
<div class="contact">
	<div class="container"><?php get_template_part('content', 'contact'); ?></div>
</div>
<?php get_footer(); ?>