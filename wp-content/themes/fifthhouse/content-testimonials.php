<?php
$args = array(
	'post_type'	 		=> 'testimonials',
	'posts_per_page' 	=> 3,
	'orderby'			=> 'date',
	'order'				=> 'DESC'
);
$testimonials = new WP_Query($args);
?>
<div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<?php
		if($testimonials->have_posts()):
			while($testimonials->have_posts()):
				$testimonials->the_post();
		?>
		<li data-target="#testimonial-carousel" data-slide-to="<?php echo $testimonials->current_post; ?>" class="<?php echo ($testimonials->current_post == 0) ? 'active' : ''; ?>"></li>
		<?php
			endwhile;
		endif;
		?>
	</ol>
	<div class="carousel-inner" role="listbox">
		<?php
		if($testimonials->have_posts()):
			while($testimonials->have_posts()):
				$testimonials->the_post();
		?>
		<div class="item <?php echo ($testimonials->current_post == 0) ? 'active' : ''; ?>">
			<div class="carousel-caption">
				<?php the_content(); ?>
				<p class="testi-name"><?php the_title(); ?></p>
			</div>
		</div>
		<?php
			endwhile;
		endif;
		?>
	</div>
</div>