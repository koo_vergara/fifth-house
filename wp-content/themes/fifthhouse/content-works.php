<?php
if($posts->have_posts()):
	while($posts->have_posts()):
		$posts->the_post();
?>
<div class="col-sm-4 work">
	<section class="work-info">
		<div class="row">
			<div class="col-sm-12">
				<div class="work-img">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
				</div>
			</div>
		</div>
		<div class="row work-desc">
			<div class="col-sm-12">
				<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
				<p><?php echo wp_trim_words(get_the_content(), 20, '...'); ?></p>
			</div>
		</div>
	</section>
</div>
<?php
	endwhile;
endif;
?>