<?php wp_footer(); ?>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-sm-8 footer-about">
						<h4>About Us</h4>
						<p>Aenean mattis augue in arcu tempor bibendum. Nam euismod facilisis magna, quis pharetra turpis molestie eu. Integer feugiat arcu sit amet leo ullamcorper feugiat efficitur. Mauris sed odio tincidunt, volutpat gue.</p>
						<p><a href=""><img src="<?php echo get_template_directory_uri().'/images/fb_icon.png'; ?>"></a></p>
					</div>
					<div class="col-sm-4">
						<h4>Instagram</h4>
						<?php echo do_shortcode('[instagram-feed]'); ?>
					</div>
				</div>
			</div>
		</footer>
	</body>
</html>