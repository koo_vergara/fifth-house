<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fifthhouse_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.t5tAlx<^m]gs]* eK,^AL/wI]}{!i!Tjdr-sq NcpsjU0:X@c&!xJ4y3-&EjN[b');
define('SECURE_AUTH_KEY',  'k0Qs K/LA=HxFP-CmP:v:B<_#@|D}P} nWan<l6^R2:d1V0IuuhIwFO^BpHZWu_O');
define('LOGGED_IN_KEY',    'a[R$-0fEvne+w^m|*ox:5DGJuWv-*n734y,x-Or*m4=5P`y)Pw ny|-qF_ekwlqM');
define('NONCE_KEY',        'xH*}j:ycRGpkIP}>,+2,d*=^uLy &It=DCFvc.Mzh(ZkvL>o4,~s4]jXy?hcTr>f');
define('AUTH_SALT',        'Rj`afs-a:Bz?Z/hXN?iw0*.HKjWYSG#6%|^p=3rY)sL3J]-%0I,}k^:5atq>j!:U');
define('SECURE_AUTH_SALT', 'oP|~&3y^{3 ]3r rM)Mts^U_AU%O5HfBQ-guir-#ofTHH=:Ds:NS}P1qkwj?A_OM');
define('LOGGED_IN_SALT',   'W6loO*W/sEum.j>nCU23Y4VhWk;UGLlpbZotSV~**D&pG9p7-zJJ(<kd6wPdayK*');
define('NONCE_SALT',       '5R968K2kG;jHbZLj-|zsU8rPrOk?#HsIB|7#jn5O~@[c?!4tZh $B]N YuD^ymzy');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
